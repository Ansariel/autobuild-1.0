import os
import platform

def getOverrideName():
  system = platform.system().lower()
  if system.find( "linux" ) == 0:
    return "package_override_gcc.ini"

  return "package_override_vc12.ini"

def readOverrideIni():
  import ConfigParser
  cfg = ConfigParser.ConfigParser()
  if os.environ.has_key( "PACKAGE_OVERRIDE" ) and os.path.isfile( os.environ[ "PACKAGE_OVERRIDE" ] ):
    cfg.read( os.environ[ "PACKAGE_OVERRIDE" ] )
  cfg.read( os.path.join( "..", "..", getOverrideName() ) )
  cfg.read( os.path.join( "..", getOverrideName() ) )
  cfg.read( getOverrideName() )
  return cfg

def applyOverrideToPackage( package_description ):
  cfg = readOverrideIni()
  if not package_description.has_key( 'name' ):
    return
  if not cfg.has_section( package_description[ 'name' ].lower() ):
    return

  for ( sysname, sysval ) in cfg.items( package_description[ 'name' ].lower() ):
    if package_description.has_key( 'platforms' ) and package_description[ 'platforms' ].has_key( sysname ):
      if sysval.find( "|" ) > 0:
        ( kName, kValue ) = sysval.split( "|" )
        if package_description[ 'platforms' ][ sysname ].has_key( kName ):
          package_description[ 'platforms' ][ sysname ][ kName ] = kValue
        else:
          if package_description[ 'platforms' ][ sysname ].has_key( kName ):
            del package_description[ 'platforms' ][ sysname ]


def applyOverrideToInstallable( name, package ):
  cfg = readOverrideIni()
  if not cfg.has_section( name.lower() ):
    return

  for ( sysname, syspkg ) in cfg.items( name.lower() ):
    if package.has_key( 'platforms' ) and  package[ 'platforms' ].has_key( sysname ):
      if syspkg.find( "|" ) > 0:
        package[ 'platforms' ][ sysname ][ 'archive' ][ 'url' ] = syspkg.split("|")[0]
        package[ 'platforms' ][ sysname ][ 'archive' ][ 'hash' ]  = syspkg.split("|")[1]
      elif len( syspkg ) > 1:
        package[ 'platforms' ][ sysname ][ 'archive' ][ 'url' ] = syspkg

        if package[ 'platforms' ][ sysname ][ 'archive' ].has_key( 'hash' ):
          del package[ 'platforms' ][ sysname ][ 'archive' ][ 'hash' ]
      else:
        del package[ 'platforms' ][ sysname ]

def changeWindows64Platform( package ):
  if not package.has_key( 'platforms' ):
    return
  if not package[ 'platforms' ].has_key( 'windows' ):
    return
  if not package[ 'platforms' ][ 'windows' ].has_key( 'configurations' ):
    return

  for cfg in package[ 'platforms' ][ 'windows' ] [ 'configurations' ]:
    if package[ 'platforms' ][ 'windows' ] [ 'configurations' ][ cfg ].has_key( 'build' ):
      if package[ 'platforms' ][ 'windows' ] [ 'configurations' ][ cfg ][ 'build' ].has_key( 'options' ):
        for opt in range( len( package[ 'platforms' ][ 'windows' ] [ 'configurations' ][ cfg ][ 'build' ][ 'options' ] ) ):
          if package[ 'platforms' ][ 'windows' ] [ 'configurations' ][ cfg ][ 'build' ][ 'options' ][ opt ] == '/p:Platform=Win32':
            package[ 'platforms' ][ 'windows' ] [ 'configurations' ][ cfg ][ 'build' ][ 'options' ][ opt ] = "/p:Platform=x64"


def sourceEnvironment( aIs64Bit ):
    if aIs64Bit:
        os.environ['ND_AUTOBUILD_ARCH' ] = 'x64'
        os.environ['ND_AUTOBUILD_GCC_ARCH_FLAG' ] = '-m64 -fPIC'

        if 0 == os.environ['AUTOBUILD_PLATFORM'].lower().find( "linux" ):
            os.environ['TARGET_OPTS' ] = '-m64 -fPIC'

        if 0 == os.environ['AUTOBUILD_PLATFORM'].lower().find( "windows" ):
            os.environ['ND_SLN_ARCH' ] = 'x64'
            vsVer = os.environ.get('AUTOBUILD_VSVER', '120')
            assert( vsVer == "100" or vsVer == "120" )
            if vsVer == "120":
                os.environ['ND_CMAKE_GENERATOR' ] = "Visual Studio 12 Win64"
            else:
                os.environ['ND_CMAKE_GENERATOR' ] = "Visual Studio 10 Win64"
    else:
        os.environ['ND_AUTOBUILD_ARCH' ] = 'x86'
        os.environ['ND_AUTOBUILD_GCC_ARCH_FLAG' ] = '-m32'

        if 0 == os.environ['AUTOBUILD_PLATFORM'].lower().find( "linux" ):
            os.environ['TARGET_OPTS' ] = '-m32'

        if 0 == os.environ['AUTOBUILD_PLATFORM'].lower().find( "windows" ):
            os.environ['ND_SLN_ARCH' ] = 'Win32'
            vsVer = os.environ.get('AUTOBUILD_VSVER', '120')
            assert( vsVer == "100" or vsVer == "120" )
            if vsVer == "120":
                os.environ['ND_CMAKE_GENERATOR' ] = "Visual Studio 12"
            else:
                os.environ['ND_CMAKE_GENERATOR' ] = "Visual Studio 10"
