import configfile
from autobuild_base import AutobuildBase

class AutobuildTool(AutobuildBase):

    def get_details(self):
        return dict(name=self.name_from_file(__file__),
                    description="Take a snapshot of the current directory tree and save it to .dirstate.json.")

    def register(self, parser):
        parser.description = "Take a snapshot of the current directory tree and save it to .dirstate.json."

    def run(self, args ):
        from os.path import join, curdir, isdir
        from os import walk
        from json import dumps
        
        hgName = join( curdir, ".hg" )
        dirState = {}
        dirState[ "dirs" ] = []
        dirState[ "files" ] = []
        for r, dirs, files in walk( curdir ):
            if r[ 0: len( hgName ) ] == hgName:
                continue

            for f in files:
                dirState[ "files" ].append( join( r, f ) )
            for d  in dirs:
                if join( r, d ) != hgName:
                    dirState[ "dirs" ].append( join( r, d ) )

        open( ".dirstate.json", "w" ).write( dumps( dirState, indent=1 ) )
        
