import configfile
from autobuild_base import AutobuildBase

class AutobuildTool(AutobuildBase):

    def get_details(self):
        return dict(name=self.name_from_file(__file__),
                    description="Take a snapshot of the current directory tree and save it to .dirstate.json.")

    def register(self, parser):
        parser.description = "Take a snapshot of the current directory tree and save it to .dirstate.json."

    def run(self, args ):
        from os.path import join, curdir, isdir, isfile
        from os import walk, unlink, rmdir
        from json import loads

        if not isfile( ".dirstate.json"  ):
            print( "No recorded dirstate. run autobuild snapshot" )
            return 1
        
        hgName = join( curdir, ".hg" )
        stateName = join( curdir, ".dirstate.json" )
        dirStateOld = loads( open( ".dirstate.json", "r" ).read() )
        curDirs = []

        for r, dirs, files in walk( curdir ):
            if r[ 0: len( hgName ) ] == hgName:
                continue

            for f in files:
                name = join( r, f )
                if name not in dirStateOld[ "files" ] and name != stateName:
                    unlink( name )
                elif name != stateName:
                    dirStateOld[ "files" ].remove( name )

            for d  in dirs:
                name = join( r, d )
                if name != hgName:
                    curDirs.append( name )

        print( "Purging dirs" )
        for d  in curDirs:
            if d not in dirStateOld[ "dirs" ]:
                print( "Extra dir %s" % d )

        
